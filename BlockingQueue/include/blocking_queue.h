#ifndef BLOCKING_QUEUE_H
#define BLOCKING_QUEUE_H

template <typename Queue,
          typename ConditionVariable,
          typename LockGuard>
class blocking_queue
{
public:
    using Element = typename Queue::value_type;

    void push_and_notify_one(Element task)
    {
        {
            LockGuard lock(mutex);

            queue.push(task);
        }

        conditionVariable.notify_one();
    }

    Element wait_and_pop()
    {
        LockGuard lock(mutex);

        conditionVariable.wait(lock, [&]{ return !queue.empty(); });

        Element task = queue.front();

        queue.pop();

        return task;
    }

    void push_and_notify_all(Element task)
    {
        {
            LockGuard lock(mutex);

            queue.push(task);
        }

        conditionVariable.notify_all();
    }

private:
    Queue queue;
    typename LockGuard::mutex_type mutex;
    ConditionVariable conditionVariable;
};

#endif // BLOCKING_QUEUE_H

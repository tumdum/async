#ifndef MOCKCONDITIONVARIABLE_H
#define MOCKCONDITIONVARIABLE_H

#include <gmock/gmock.h>

template <typename lock_guard_type>
class MockConditionVariable
{
public:
    MOCK_METHOD1_T(wait, void(lock_guard_type& lock));
    MOCK_METHOD0_T(notify_one, void());
    MOCK_METHOD0_T(notify_all, void());
};

#endif // MOCKCONDITIONVARIABLE_H

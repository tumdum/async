#ifndef MOCKQUEUE_H
#define MOCKQUEUE_H

#include <gmock/gmock.h>

template <typename value_type>
class MockQueue
{
public:
    MOCK_METHOD1_T(push, void(const value_type&));
    MOCK_METHOD0_T(pop, void());
    MOCK_METHOD0_T(front, value_type&());
    MOCK_CONST_METHOD0_T(empty, bool());
};

#endif // MOCKQUEUE_H

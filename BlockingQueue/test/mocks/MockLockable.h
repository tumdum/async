#ifndef MOCKLOCKABLE_H
#define MOCKLOCKABLE_H

#include <gmock/gmock.h>

class MockLockable
{
public:
    MOCK_CONST_METHOD0(lock, void());
    MOCK_CONST_METHOD0(unlock, void());
};

#endif // MOCKLOCKABLE_H

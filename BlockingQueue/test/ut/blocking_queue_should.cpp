#include <gtest/gtest.h>
#include "blocking_queue.h"
#include "MockLockable.h"
#include "MockConditionVariable.h"
#include "MockQueue.h"

using testing::StrictMock;
using testing::InSequence;
using testing::Return;
using testing::ReturnRef;
using testing::_;

struct FakeLockGuard
{
    FakeLockGuard(MockLockable &lockable)
        : lockable(lockable)
    {
        lockable.lock();
    }

    ~FakeLockGuard()
    {
        lockable.unlock();
    }

private:
    MockLockable& lockable;
};

struct blocking_queue_should : testing::Test
{
    StrictMock< MockQueue<int> > mockQueue;
    StrictMock< MockLockable > mockMutex;
    StrictMock< MockConditionVariable<FakeLockGuard> > mockConditionVariable;

    blocking_queue<int, MockQueue<int>,
        MockLockable,
        MockConditionVariable<FakeLockGuard>,
        FakeLockGuard > blockingQueue;

    int someIntegerValue;

    blocking_queue_should()
        : blockingQueue(mockQueue, mockMutex, mockConditionVariable),
          someIntegerValue(12345)
    {}
};

TEST_F(blocking_queue_should, push_some_value_and_notify_one_thread_about_it)
{
    {
        InSequence seq;

        EXPECT_CALL(mockMutex, lock());

        EXPECT_CALL(mockQueue, push(someIntegerValue));

        EXPECT_CALL(mockMutex, unlock());

        EXPECT_CALL(mockConditionVariable, notify_one());
    }

    blockingQueue.push_and_notify_one(someIntegerValue);
}

TEST_F(blocking_queue_should, pop_value_if_there_exists_any)
{
    {
        InSequence seq;

        EXPECT_CALL(mockMutex, lock());

        EXPECT_CALL(mockQueue, empty())
                .WillOnce(Return(false));

        EXPECT_CALL(mockQueue, front())
                .WillOnce(ReturnRef(someIntegerValue));

        EXPECT_CALL(mockQueue, pop());

        EXPECT_CALL(mockMutex, unlock());
    }

    EXPECT_EQ(someIntegerValue, blockingQueue.wait_and_pop());
}

TEST_F(blocking_queue_should, wait_on_value_if_queue_is_empty)
{
    {
        InSequence seq;

        EXPECT_CALL(mockMutex, lock());

        EXPECT_CALL(mockQueue, empty())
                .WillOnce(Return(true))
                .RetiresOnSaturation();

        EXPECT_CALL(mockConditionVariable, wait(_));

        EXPECT_CALL(mockQueue, empty())
                .WillOnce(Return(false));

        EXPECT_CALL(mockQueue, front())
                .WillOnce(ReturnRef(someIntegerValue));

        EXPECT_CALL(mockQueue, pop());

        EXPECT_CALL(mockMutex, unlock());
    }

    EXPECT_EQ(someIntegerValue, blockingQueue.wait_and_pop());
}

TEST_F(blocking_queue_should, push_some_value_and_notify_all_threads_about_it)
{
    {
        InSequence seq;

        EXPECT_CALL(mockMutex, lock());

        EXPECT_CALL(mockQueue, push((someIntegerValue)));

        EXPECT_CALL(mockMutex, unlock());

        EXPECT_CALL(mockConditionVariable, notify_all());
    }

    blockingQueue.push_and_notify_all(someIntegerValue);
}

#include <gtest/gtest.h>
#include "MockTaskWithDie.h"
#include "SynchronousCompletion.h"

using testing::InSequence;

struct SynchronousCompletionShould : testing::Test
{
    SynchronousCompletion synchronousCompletion;
};

TEST_F(SynchronousCompletionShould, BeAbleToCompleteAndDestroyExistingTask)
{
    MockTaskWithDie* mockTaskPtr = new MockTaskWithDie();

    {
        InSequence seq;

        EXPECT_CALL(*mockTaskPtr, onComplete());

        EXPECT_CALL(*mockTaskPtr, Die());

    }

    synchronousCompletion.completeTask(mockTaskPtr);
}

TEST_F(SynchronousCompletionShould, ThrowExceptionWhenTaskIsNull)
{
    EXPECT_THROW(synchronousCompletion.completeTask(NULL), std::exception);
}


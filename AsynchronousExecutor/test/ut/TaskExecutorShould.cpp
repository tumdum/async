#include <gtest/gtest.h>
#include "MockBlockingTaskQueue.h"
#include "MockNotifier.h"
#include "MockTask.h"
#include "TaskExecutorImpl.h"

using testing::Return;
using testing::InSequence;

struct TaskExecutorShould : testing::Test
{
    TaskExecutorShould()
        : taskExecutor(mockBlockingTaskQueue, mockNotifier)
    {}

    MockBlockingTaskQueue mockBlockingTaskQueue;
    MockNotifier mockNotifier;
    TaskExecutorImpl taskExecutor;
};

TEST_F(TaskExecutorShould, ExecutePendingTasksFromQueueAndNotifyWhenItsDone)
{
    MockTask mockTask;

    EXPECT_CALL(mockBlockingTaskQueue, waitForNextPendingTaskToExecute())
            .WillOnce(Return(&mockTask))
            .WillOnce(Return(static_cast<Task*>(NULL)));

    {
        InSequence seq;

        EXPECT_CALL(mockTask, onExecute());

        EXPECT_CALL(mockNotifier, notifyTaskCompleted(&mockTask));
    }

    taskExecutor.startExecutionLoop();

    taskExecutor.join();
}

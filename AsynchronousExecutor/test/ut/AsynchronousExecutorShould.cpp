#include <gtest/gtest.h>
#include "MockBlockingTaskQueue.h"
#include "MockTaskWithDie.h"
#include "MockTaskExecutor.h"
#include "MockTaskExecutorFactory.h"
#include "AsynchronousExecutorImpl.h"

using testing::Mock;
using testing::Return;
using testing::InSequence;

struct AsynchronousExecutorShould : testing::Test
{
    AsynchronousExecutorShould()
        : mockTaskExecutorPtr(NULL),
          mockTaskExecutorPtr2(NULL),
          asynchronousExecutor(mockBlockingTaskQueue, mockTaskExecutorFactory)
    {}

    void configureTaskExecutorFactoryWithTwoExecutionLoops()
    {
        mockTaskExecutorPtr = new MockTaskExecutor();
        mockTaskExecutorPtr2 = new MockTaskExecutor();

        EXPECT_CALL(*mockTaskExecutorPtr, startExecutionLoop());
        EXPECT_CALL(*mockTaskExecutorPtr2, startExecutionLoop());

        EXPECT_CALL(mockTaskExecutorFactory, createTaskExecutor())
                .WillOnce(Return(mockTaskExecutorPtr))
                .WillOnce(Return(mockTaskExecutorPtr2));
    }

    void verifyExpectationsAndDestroyExecutionLoops()
    {
        ASSERT_TRUE(Mock::VerifyAndClearExpectations(mockTaskExecutorPtr));
        ASSERT_TRUE(Mock::VerifyAndClearExpectations(mockTaskExecutorPtr2));

        EXPECT_CALL(*mockTaskExecutorPtr, Die());
        EXPECT_CALL(*mockTaskExecutorPtr2, Die());

        delete mockTaskExecutorPtr;
        delete mockTaskExecutorPtr2;
    }

protected:
    MockTaskExecutor* mockTaskExecutorPtr;
    MockTaskExecutor* mockTaskExecutorPtr2;
    MockBlockingTaskQueue mockBlockingTaskQueue;
    MockTaskExecutorFactory mockTaskExecutorFactory;
    AsynchronousExecutorImpl asynchronousExecutor;
};

TEST_F(AsynchronousExecutorShould, BeAbleToCreateAndStartNumberOfTaskTaskExecutors)
{
    configureTaskExecutorFactoryWithTwoExecutionLoops();

    asynchronousExecutor.createAndStartTaskExecutors(2);

    verifyExpectationsAndDestroyExecutionLoops();
}

TEST_F(AsynchronousExecutorShould, BeAbleToJoinAndDestroyCreatedExecutors)
{
    configureTaskExecutorFactoryWithTwoExecutionLoops();

    asynchronousExecutor.createAndStartTaskExecutors(2);

    EXPECT_CALL(mockBlockingTaskQueue, scheduleShutDownTask()).Times(2);

    {
        InSequence seq;
        EXPECT_CALL(*mockTaskExecutorPtr, join());
        EXPECT_CALL(*mockTaskExecutorPtr, Die());
    }

    {
        InSequence seq;
        EXPECT_CALL(*mockTaskExecutorPtr2, join());
        EXPECT_CALL(*mockTaskExecutorPtr2, Die());
    }

    asynchronousExecutor.joinAndReleaseTaskExecutors();
}

TEST_F(AsynchronousExecutorShould, ScheduleTaskOnTaskQueueDuringExecution)
{
    Task* taskPtr = reinterpret_cast<Task*>(321);

    EXPECT_CALL(mockBlockingTaskQueue, scheduleTaskToExecute(taskPtr));

    asynchronousExecutor.execute(taskPtr);
}

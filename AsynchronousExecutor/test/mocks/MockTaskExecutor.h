#ifndef MOCKTASKEXECUTOR_H
#define MOCKTASKEXECUTOR_H

#include <gmock/gmock.h>
#include "TaskExecutor.h"

class MockTaskExecutor : public TaskExecutor
{
public:
    MOCK_METHOD0(Die, void());
    virtual ~MockTaskExecutor() { Die(); }

    MOCK_METHOD0(startExecutionLoop, void());
    MOCK_METHOD0(join, void());
};

#endif // MOCKTASKEXECUTOR_H

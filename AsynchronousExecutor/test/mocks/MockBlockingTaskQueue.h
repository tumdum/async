#ifndef MOCKBLOCKINGTASKQUEUE_H
#define MOCKBLOCKINGTASKQUEUE_H

#include <gmock/gmock.h>
#include "BlockingTaskQueue.h"

class MockBlockingTaskQueue : public BlockingTaskQueue
{
public:
    MOCK_METHOD1(scheduleTaskToExecute, void(Task*));
    MOCK_METHOD0(waitForNextPendingTaskToExecute, Task*());
    MOCK_METHOD0(scheduleShutDownTask, void());
};

#endif // MOCKBLOCKINGTASKQUEUE_H

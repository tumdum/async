#ifndef MOCKTASKWITHDIE_H
#define MOCKTASKWITHDIE_H

#include "MockTask.h"

class MockTaskWithDie : public MockTask
{
public:
    MOCK_METHOD0(Die, void());
    virtual ~MockTaskWithDie() { Die(); }
};

#endif // MOCKTASKWITHDIE_H

#ifndef MOCKNOTIFIER_H
#define MOCKNOTIFIER_H

#include <gmock/gmock.h>
#include "Notifier.h"

class MockNotifier : public Notifier
{
public:
    MOCK_METHOD1(notifyTaskCompleted, void(TaskId taskId));
};

#endif // MOCKNOTIFIER_H

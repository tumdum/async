#ifndef MOCKTASKEXECUTORFACTORY_H
#define MOCKTASKEXECUTORFACTORY_H

#include <gmock/gmock.h>
#include "TaskExecutorFactory.h"

class MockTaskExecutorFactory : public TaskExecutorFactory
{
public:
    MOCK_METHOD0(createTaskExecutor, TaskExecutor*());
};

#endif // MOCKTASKEXECUTORFACTORY_H

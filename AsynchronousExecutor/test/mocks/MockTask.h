#ifndef MOCKTASK_H
#define MOCKTASK_H

#include <gmock/gmock.h>
#include "Task.h"

class MockTask : public Task
{
public:
    MOCK_METHOD0(onExecute, void());
    MOCK_METHOD0(onComplete, void());
};

#endif // MOCKTASK_H

#include <gtest/gtest.h>
#include "AsynchronousExecutor.h"
#include "SynchronousCompletion.h"
#include "MockTaskWithDie.h"
#include "MockNotifier.h"

using testing::InSequence;

struct IntegrationTests : public testing::Test
{
    IntegrationTests()
        : asynchronousExecutor(mockNotifier)
    {}

    MockNotifier mockNotifier;

    AsynchronousExecutor asynchronousExecutor;
    SynchronousCompletion synchronousCompletion;
};

TEST_F(IntegrationTests, OneTaskShouldBeExecutedByOneWorker)
{
    MockTaskWithDie* mockTask = new MockTaskWithDie();

    {
        InSequence seq;

        EXPECT_CALL(*mockTask, onExecute());

        EXPECT_CALL(mockNotifier, notifyTaskCompleted(mockTask));

        EXPECT_CALL(*mockTask, onComplete());

        EXPECT_CALL(*mockTask, Die());
    }

    asynchronousExecutor.execute(mockTask);

    asynchronousExecutor.createAndStartTaskExecutors(1);
    asynchronousExecutor.joinAndReleaseTaskExecutors();

    synchronousCompletion.completeTask(mockTask);
}

TEST_F(IntegrationTests, TwoTasksToExecutionWithTwoWorkers)
{
    MockTaskWithDie* mockTask1 = new MockTaskWithDie();
    MockTaskWithDie* mockTask2 = new MockTaskWithDie();

    {
        InSequence seq;

        EXPECT_CALL(*mockTask1, onExecute());

        EXPECT_CALL(mockNotifier, notifyTaskCompleted(mockTask1));

        EXPECT_CALL(*mockTask1, onComplete());

        EXPECT_CALL(*mockTask1, Die());
    }

    {
        InSequence seq;

        EXPECT_CALL(*mockTask2, onExecute());

        EXPECT_CALL(mockNotifier, notifyTaskCompleted(mockTask2));

        EXPECT_CALL(*mockTask2, onComplete());

        EXPECT_CALL(*mockTask2, Die());
    }

    asynchronousExecutor.createAndStartTaskExecutors(5);

    asynchronousExecutor.execute(mockTask1);
    asynchronousExecutor.execute(mockTask2);

    asynchronousExecutor.joinAndReleaseTaskExecutors();

    synchronousCompletion.completeTask(mockTask1);
    synchronousCompletion.completeTask(mockTask2);
}

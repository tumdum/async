#ifndef COMPLETION_H
#define COMPLETION_H

#include "TaskId.h"

class Completion
{
public:
    virtual ~Completion() {}
    virtual void completeTask(TaskId taskId) = 0;
};

#endif // COMPLETION_H

#ifndef ASYNCHRONOUSEXECUTOR_H
#define ASYNCHRONOUSEXECUTOR_H

#include <memory>
#include "Executor.h"
#include "Notifier.h"

class AsynchronousExecutor : public Executor
{
public:
    AsynchronousExecutor(Notifier& notifier);
    ~AsynchronousExecutor();
    void createAndStartTaskExecutors(unsigned int numberOfExecutors);
    void execute(Task* task);
    void joinAndReleaseTaskExecutors();

private:
    class Impl;
    std::unique_ptr<Impl> _impl;
};

#endif // ASYNCHRONOUSEXECUTOR_H

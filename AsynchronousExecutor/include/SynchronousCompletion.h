#ifndef SYNCHRONOUSCOMPLETION_H
#define SYNCHRONOUSCOMPLETION_H

#include <exception>
#include "Completion.h"
#include "Task.h"

class SynchronousCompletion : public Completion
{
public:
    void completeTask(TaskId taskId)
    {
        if (taskId == NULL)
            throw std::exception();

        Task* task = reinterpret_cast<Task*>(taskId);

        task->onComplete();

        delete task;
    }
};

#endif // SYNCHRONOUSCOMPLETION_H

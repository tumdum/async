#ifndef TASK_H
#define TASK_H

class Task
{
public:
    virtual ~Task() {}
    virtual void onExecute() = 0;
    virtual void onComplete() = 0;
};

#endif // TASK_H

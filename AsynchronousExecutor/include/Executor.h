#ifndef EXECUTOR_H
#define EXECUTOR_H

#include "Task.h"

class Executor
{
public:
    virtual ~Executor() {}
    virtual void createAndStartTaskExecutors(unsigned int numberOfExecutors) = 0;
    virtual void execute(Task* task) = 0;
    virtual void joinAndReleaseTaskExecutors() = 0;
};

#endif // EXECUTOR_H

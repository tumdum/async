#ifndef NOTIFIER_H
#define NOTIFIER_H

#include "TaskId.h"

class Notifier
{
public:
    virtual ~Notifier() {}
    virtual void notifyTaskCompleted(TaskId taskId) = 0;
};

#endif // NOTIFIER_H

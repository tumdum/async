#ifndef ASYNCHRONOUSEXECUTORIMPL_H
#define ASYNCHRONOUSEXECUTORIMPL_H

#include <vector>
#include "BlockingTaskQueue.h"
#include "TaskExecutorFactory.h"
#include "Task.h"

class AsynchronousExecutorImpl
{
    typedef std::vector<TaskExecutor*> TaskExecutorContainerType;

public:
    AsynchronousExecutorImpl(BlockingTaskQueue& blockingTaskQueue,
                         TaskExecutorFactory& taskExecutorFactory);
    ~AsynchronousExecutorImpl();
    void execute(Task* task);
    void createAndStartTaskExecutors(unsigned int numberOfLoops);
    void joinAndReleaseTaskExecutors();

private:
    BlockingTaskQueue& blockingTaskQueue;
    TaskExecutorFactory& taskExecutorFactory;
    TaskExecutorContainerType listOfTaskExecutors;
};

#endif // ASYNCHRONOUSEXECUTORIMPL_H

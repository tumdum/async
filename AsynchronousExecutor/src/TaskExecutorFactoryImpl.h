#ifndef TASKEXECUTORFACTORYIMPL_H
#define TASKEXECUTORFACTORYIMPL_H

#include "TaskExecutorFactory.h"
#include "TaskExecutorImpl.h"
#include "BlockingTaskQueue.h"
#include "Notifier.h"

class TaskExecutorFactoryImpl : public TaskExecutorFactory
{
public:
    TaskExecutorFactoryImpl(BlockingTaskQueue& blockingTaskQueue, Notifier& notifier)
        : blockingTaskQueue(blockingTaskQueue), notifier(notifier)
    {

    }

    TaskExecutor* createTaskExecutor()
    {
        return new TaskExecutorImpl(blockingTaskQueue, notifier);
    }

private:
    BlockingTaskQueue& blockingTaskQueue;
    Notifier& notifier;
};

#endif // TASKEXECUTORFACTORYIMPL_H

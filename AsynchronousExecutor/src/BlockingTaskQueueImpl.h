#ifndef BLOCKINGTASKQUEUEIMPL_H
#define BLOCKINGTASKQUEUEIMPL_H

#include <queue>
#include <mutex>
#include <condition_variable>
#include "BlockingTaskQueue.h"
#include "blocking_queue.h"

class BlockingTaskQueueImpl : public BlockingTaskQueue
{
public:

    void scheduleTaskToExecute(Task* task)
    {
        blockingQueue.push_and_notify_one(task);
    }

    Task* waitForNextPendingTaskToExecute()
    {
        return blockingQueue.wait_and_pop();
    }

    void scheduleShutDownTask()
    {
         blockingQueue.push_and_notify_all(NULL);
    }

private:

    blocking_queue<std::queue<Task*>,
                   std::condition_variable,
                   std::unique_lock<std::mutex> > blockingQueue;
};

#endif // BLOCKINGTASKQUEUEIMPL_H

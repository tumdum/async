#ifndef TASKEXECUTORIMPL_H
#define TASKEXECUTORIMPL_H

#include <thread>
#include "TaskExecutor.h"
#include "BlockingTaskQueue.h"
#include "Notifier.h"

class TaskExecutorImpl : public TaskExecutor
{
public:
    TaskExecutorImpl(BlockingTaskQueue& blockingTaskQueue, Notifier& notifier);
    void startExecutionLoop();
    void join();

private:
    void executionLoop();
    void executeTaskAndNotifyWhenItsDone(Task* task);

    BlockingTaskQueue& blockingTaskQueue;
    Notifier& notifier;
    bool running;

    std::thread threadForLoop;
};

#endif // TASKEXECUTORIMPL_H

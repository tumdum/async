#include <functional>
#include "TaskExecutorImpl.h"

TaskExecutorImpl::TaskExecutorImpl(BlockingTaskQueue& blockingTaskQueue, Notifier& notifier)
    : blockingTaskQueue(blockingTaskQueue), notifier(notifier), running(true)
{}

void TaskExecutorImpl::startExecutionLoop()
{
    threadForLoop =
            std::thread(
                std::bind(
                    &TaskExecutorImpl::executionLoop,
                    this
                )
            );
}

void TaskExecutorImpl::executionLoop()
{
    while (running)
    {
        Task* task = blockingTaskQueue.waitForNextPendingTaskToExecute();

        if (task)
        {
            executeTaskAndNotifyWhenItsDone(task);
        }
        else
            running = false;
    }

}

void TaskExecutorImpl::executeTaskAndNotifyWhenItsDone(Task* task)
{
    task->onExecute();

    notifier.notifyTaskCompleted(task);
}

void TaskExecutorImpl::join()
{
    threadForLoop.join();
}

#ifndef BLOCKINGTASKQUEUE_H
#define BLOCKINGTASKQUEUE_H

#include "Task.h"

class BlockingTaskQueue
{
public:
    virtual ~BlockingTaskQueue() {}
    virtual void scheduleTaskToExecute(Task* task) = 0;
    virtual Task* waitForNextPendingTaskToExecute() = 0;
    virtual void scheduleShutDownTask() = 0;
};

#endif // BLOCKINGTASKQUEUE_H

#ifndef TASKEXECUTOR_H
#define TASKEXECUTOR_H

class TaskExecutor
{
public:
    virtual ~TaskExecutor() {}
    virtual void startExecutionLoop() = 0;
    virtual void join() = 0;
};

#endif // TASKEXECUTOR_H

#ifndef TASKEXECUTORFACTORY_H
#define TASKEXECUTORFACTORY_H

#include "TaskExecutor.h"

class TaskExecutorFactory
{
public:
    virtual ~TaskExecutorFactory() {}
    virtual TaskExecutor* createTaskExecutor() = 0;
};

#endif // TASKEXECUTORFACTORY_H

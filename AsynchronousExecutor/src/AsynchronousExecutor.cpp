#include "AsynchronousExecutor.h"
#include "TaskExecutorFactoryImpl.h"
#include "BlockingTaskQueueImpl.h"
#include "AsynchronousExecutorImpl.h"

class AsynchronousExecutor::Impl
{
public:
    Impl(Notifier& notifier)
        : taskExecutorFactory(blockingTaskQueue, notifier),
          asyncExecutorImpl(blockingTaskQueue, taskExecutorFactory)
    {}

    void createAndStartTaskExecutors(unsigned int numberOfExecutors)
    {
        asyncExecutorImpl.createAndStartTaskExecutors(numberOfExecutors);
    }

    void execute(Task* task)
    {
        asyncExecutorImpl.execute(task);
    }

    void joinAndReleaseTaskExecutors()
    {
        asyncExecutorImpl.joinAndReleaseTaskExecutors();
    }

private:
    BlockingTaskQueueImpl blockingTaskQueue;
    TaskExecutorFactoryImpl taskExecutorFactory;
    AsynchronousExecutorImpl asyncExecutorImpl;
};

AsynchronousExecutor::AsynchronousExecutor(Notifier& notifier)
    : _impl(new Impl(notifier))
{}

AsynchronousExecutor::~AsynchronousExecutor()
{}

void AsynchronousExecutor::createAndStartTaskExecutors(unsigned int numberOfExecutors)
{
    _impl->createAndStartTaskExecutors(numberOfExecutors);
}

void AsynchronousExecutor::execute(Task* task)
{
    _impl->execute(task);
}

void AsynchronousExecutor::joinAndReleaseTaskExecutors()
{
    _impl->joinAndReleaseTaskExecutors();
}

#include "AsynchronousExecutorImpl.h"

AsynchronousExecutorImpl::AsynchronousExecutorImpl(BlockingTaskQueue& blockingTaskQueue,
                                           TaskExecutorFactory& taskExecutorFactory)
    : blockingTaskQueue(blockingTaskQueue),
      taskExecutorFactory(taskExecutorFactory)
{}

AsynchronousExecutorImpl::~AsynchronousExecutorImpl()
{

}

void AsynchronousExecutorImpl::createAndStartTaskExecutors(unsigned int numberOfLoops)
{
    for (unsigned int i = 0; i < numberOfLoops; i++)
    {
        TaskExecutor* taskExecutor
                = taskExecutorFactory.createTaskExecutor();

        taskExecutor->startExecutionLoop();

        listOfTaskExecutors.push_back(taskExecutor);
    }
}

void AsynchronousExecutorImpl::joinAndReleaseTaskExecutors()
{
    TaskExecutorContainerType::iterator it;
    for(it = listOfTaskExecutors.begin(); it != listOfTaskExecutors.end(); it++)
    {
        blockingTaskQueue.scheduleShutDownTask();
    }

    for(it = listOfTaskExecutors.begin(); it != listOfTaskExecutors.end(); it++)
    {
        (*it)->join();
        delete *it;
    }

    listOfTaskExecutors.clear();
}

void AsynchronousExecutorImpl::execute(Task* task)
{
    blockingTaskQueue.scheduleTaskToExecute(task);
}

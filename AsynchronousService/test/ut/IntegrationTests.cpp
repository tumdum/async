#include <gtest/gtest.h>
#include "MessageQueueImpl.h"
#include "MockTaskWithDie.h"
#include "NotifierImpl.h"
#include "AsynchronousExecutor.h"
#include "SynchronousCompletion.h"
#include "DispatcherImpl.h"
#include "MainMessageLoop.h"
#include <unistd.h>

using std::unique_ptr;
using testing::InSequence;

class TaskController
{
public:
    TaskController(AsynchronousExecutor& asynchronousExecutor)
        : asynchronousExecutor(asynchronousExecutor)
    {}

    void addTaskToExecute(Task* task)
    {
        taskToExecute = task;
    }

    void runParallelTasks()
    {
        asynchronousExecutor.execute(taskToExecute);
    }

private:
    AsynchronousExecutor& asynchronousExecutor;
    Task* taskToExecute;
};

ACTION_P(ShutDownMessageQueue, messageQueue)
{
    messageQueue->put( {MessageId::ShutDownMessage, nullptr} );
}

struct IntegrationTests : testing::Test
{
    IntegrationTests()
        : notifier(messageQueue),
          asynchronousExecutor(notifier),
          mainMessageLoop(messageQueue, synchronousCompletion, dispatcher)
    {}

    void SetUp()
    {
        asynchronousExecutor.createAndStartTaskExecutors(1);
    }

    void TearDown()
    {
        asynchronousExecutor.joinAndReleaseTaskExecutors();
    }

    MessageQueueImpl messageQueue;
    NotifierImpl notifier;
    AsynchronousExecutor asynchronousExecutor;
    SynchronousCompletion synchronousCompletion;
    DispatcherImpl dispatcher;
    MainMessageLoop mainMessageLoop;
};

TEST_F(IntegrationTests, OneTaskExecutionWithOneWorker)
{
    TaskController taskController(asynchronousExecutor);

    dispatcher.bind(MessageId::RunParallelTasksMessage, [&] { taskController.runParallelTasks(); });

    MockTaskWithDie* mockTask = new MockTaskWithDie();

    {
        InSequence seq;

        EXPECT_CALL(*mockTask, onExecute());

        EXPECT_CALL(*mockTask, onComplete());

        EXPECT_CALL(*mockTask, Die()).WillOnce(ShutDownMessageQueue(&messageQueue));
    }

    taskController.addTaskToExecute(mockTask);

    messageQueue.put( {MessageId::RunParallelTasksMessage, nullptr});

    mainMessageLoop.dispatchMessagesOrCompleteTasksUntilShutDown();
}

#include <gmock/gmock.h>
#include "NotifierImpl.h"
#include "MockMessageQueue.h"
#include "Message.h"
#include "Task.h"

MATCHER_P(CompleteMessage, taskId, "")
{
    return (arg.id == MessageId::CompleteTaskMessage) && (arg.customData == taskId);
}

TEST(NotifierShould, PutCompletionMessageIntoMessageQueue)
{
    MockMessageQueue<Message> mockMessageQueue;
    NotifierImpl notifier(mockMessageQueue);

    Task* taskPtr = reinterpret_cast<Task*>(54321);

    EXPECT_CALL(mockMessageQueue, put(CompleteMessage(taskPtr)));

    notifier.notifyTaskCompleted(taskPtr);
}

#include <gmock/gmock.h>
#include "DispatcherImpl.h"

using testing::InSequence;

class MockReactor
{
public:
    MOCK_METHOD0(anyPurpose, void());
    MOCK_METHOD0(runParallelTasks, void());
};

TEST(DispatcherShould, ForwardMessagesToRelatedBindMethods)
{
    MockReactor mockReactor;

    {
        InSequence seq;
        EXPECT_CALL(mockReactor, anyPurpose());
        EXPECT_CALL(mockReactor, runParallelTasks());
    }

    DispatcherImpl dispatcher;
    
    dispatcher.bind(MessageId::AnyPurposeMessage, [&]{ mockReactor.anyPurpose(); });

    dispatcher.bind(MessageId::RunParallelTasksMessage, [&]{ mockReactor.runParallelTasks(); });

    dispatcher.dispatch( Message{MessageId::AnyPurposeMessage, nullptr} );
    dispatcher.dispatch( Message{MessageId::RunParallelTasksMessage, nullptr} );
}

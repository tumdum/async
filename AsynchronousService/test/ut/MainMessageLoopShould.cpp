#include <gtest/gtest.h>
#include "Message.h"
#include "MockMessageQueue.h"
#include "MockDispatcher.h"
#include "MockCompletion.h"
#include "MainMessageLoop.h"

using testing::Return;
using testing::StrictMock;

struct MainMessageLoopShould : public testing::Test
{
    MainMessageLoopShould()
        : mainMessageLoop(mockMessageQueue, mockCompletion, mockDispatcher),
          shutdownMessage{MessageId::ShutDownMessage, 0}
    {}

    void SetUp()
    {
        EXPECT_CALL(mockMessageQueue, get())
                .WillOnce(Return(shutdownMessage));
    }

    MockMessageQueue<Message> mockMessageQueue;

    StrictMock<MockDispatcher> mockDispatcher;
    StrictMock<MockCompletion> mockCompletion;

    MainMessageLoop mainMessageLoop;
    Message shutdownMessage;
};

TEST_F(MainMessageLoopShould, FinishExecutionAfterReceivingShutDownMessage)
{
    mainMessageLoop.dispatchMessagesOrCompleteTasksUntilShutDown();
    SUCCEED();
}

TEST_F(MainMessageLoopShould, ForwardTaskIdFromCompletionMessageToCompletionClass)
{
    TaskId someTaskId = reinterpret_cast<void*>(123);

    Message message{MessageId::CompleteTaskMessage, someTaskId};

    EXPECT_CALL(mockMessageQueue, get())
            .WillOnce(Return(message))
            .RetiresOnSaturation();

    EXPECT_CALL(mockCompletion, completeTask(someTaskId));

    mainMessageLoop.dispatchMessagesOrCompleteTasksUntilShutDown();
}

MATCHER_P(MessageMatcher, message, "")
{
    return (arg.id == message.id) && (arg.customData == message.customData);
}

TEST_F(MainMessageLoopShould, ForwardOtherMessagesToDistacher)
{
    Message message{MessageId::AnyPurposeMessage, 0};

    EXPECT_CALL(mockMessageQueue, get())
            .WillOnce(Return(message))
            .WillOnce(Return(message))
            .RetiresOnSaturation();

    EXPECT_CALL(mockDispatcher, dispatch(MessageMatcher(message)))
            .Times(2);

    mainMessageLoop.dispatchMessagesOrCompleteTasksUntilShutDown();
}

#ifndef MOCKEXECUTOR_H
#define MOCKEXECUTOR_H

#include <gmock/gmock.h>
#include "Executor.h"

class MockExecutor : public Executor
{
public:
    MOCK_METHOD1(createAndStartTaskExecutors, void(unsigned int));
    MOCK_METHOD1(execute, void(Task*));
    MOCK_METHOD0(joinAndReleaseTaskExecutors, void(void));
};

#endif // MOCKEXECUTOR_H

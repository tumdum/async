#ifndef MOCKMESSAGEQUEUE_H
#define MOCKMESSAGEQUEUE_H

#include <gmock/gmock.h>
#include "MessageQueue.h"

template <typename T>
class MockMessageQueue : public MessageQueue<T>
{
public:
    MOCK_METHOD1_T(put, void(T));
    MOCK_METHOD0_T(get, T());
};

#endif // MOCKMESSAGEQUEUE_H

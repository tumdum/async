#ifndef MOCKCOMPLETION_H
#define MOCKCOMPLETION_H

#include <gmock/gmock.h>
#include "Completion.h"

class MockCompletion : public Completion
{
public:
    MOCK_METHOD1(completeTask, void(TaskId));
};

#endif // MOCKCOMPLETION_H

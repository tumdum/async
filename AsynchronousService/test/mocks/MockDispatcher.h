#ifndef MOCKDISPATCHER_H
#define MOCKDISPATCHER_H

#include <gmock/gmock.h>
#include "Dispatcher.h"

class MockDispatcher : public Dispatcher
{
public:
    MOCK_METHOD1(dispatch, void(const Message&));
};

#endif // MOCKDISPATCHER_H

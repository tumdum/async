#ifndef NOTIFIERIMPL_H
#define NOTIFIERIMPL_H

#include "Notifier.h"
#include "MessageQueue.h"
#include "Message.h"

class NotifierImpl : public Notifier
{
public:
    NotifierImpl(MessageQueue<Message>& messageQueue)
        : messageQueue(messageQueue)
    {}

    void notifyTaskCompleted(TaskId taskId)
    {
        Message message;

        message.id = MessageId::CompleteTaskMessage;
        message.customData = taskId;

        messageQueue.put(message);
    }
private:
    MessageQueue<Message>& messageQueue;

};

#endif // NOTIFIERIMPL_H

#ifndef MAINMESSAGELOOP_H
#define MAINMESSAGELOOP_H

#include "Message.h"
#include "MessageQueue.h"
#include "Completion.h"
#include "Dispatcher.h"

class MainMessageLoop
{
public:
    MainMessageLoop(MessageQueue<Message>& messageQueue,
                    Completion& completion,
                    Dispatcher& dispatcher)
        : messageQueue(messageQueue),
          completion(completion),
          dispatcher(dispatcher)
    {}

    void dispatchMessagesOrCompleteTasksUntilShutDown()
    {
        Message message = messageQueue.get();

        while (message.id != MessageId::ShutDownMessage)
        {
            if (message.id == MessageId::CompleteTaskMessage)
                completion.completeTask(message.customData);
            else
                dispatcher.dispatch(message);

            message = messageQueue.get();
        }
    }

    MessageQueue<Message>& messageQueue;
    Completion& completion;
    Dispatcher& dispatcher;
};

#endif // MAINMESSAGELOOP_H

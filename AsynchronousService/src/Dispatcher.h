#ifndef DISPATCHER_H
#define DISPATCHER_H

#include "Message.h"

class Dispatcher
{
public:
    virtual ~Dispatcher() {}
    virtual void dispatch(const Message& message) = 0;
};


#endif // DISPATCHER_H

#ifndef MESSAGE_H
#define MESSAGE_H

#include "MessageIdentifiers.h"

struct Message
{
    MessageId id;
    void* customData;
};

#endif // MESSAGE_H

#ifndef MESSAGEQUEUEIMPL_H
#define MESSAGEQUEUEIMPL_H

#include <queue>
#include <mutex>
#include <condition_variable>
#include "Message.h"
#include "MessageQueue.h"
#include "blocking_queue.h"

class MessageQueueImpl : public MessageQueue<Message>
{
public:
    void put(Message message)
    {
        blockingQueue.push_and_notify_one(message);
    }

    Message get()
    {
        return blockingQueue.wait_and_pop();
    }

private:

    blocking_queue<std::queue<Message>,
                   std::condition_variable,
                   std::unique_lock<std::mutex> > blockingQueue;
};

#endif // MESSAGEQUEUEIMPL_H

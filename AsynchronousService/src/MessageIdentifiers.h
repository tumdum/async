#ifndef MESSAGEIDENTIFIERS_H
#define MESSAGEIDENTIFIERS_H

enum class MessageId
{
    ShutDownMessage,
    CompleteTaskMessage,
    AnyPurposeMessage,
    RunParallelTasksMessage,
    Count
};

#endif // MESSAGEIDENTIFIERS_H

#ifndef MESSAGEQUEUE_H
#define MESSAGEQUEUE_H

template<typename T>
class MessageQueue
{
public:
    virtual ~MessageQueue() {}
    virtual void put(T message) = 0;
    virtual T get() = 0;
};

#endif // MESSAGEQUEUE_H

#ifndef DISPATCHERIMPL_H
#define DISPATCHERIMPL_H

#include <vector>
#include <functional>
#include <utility>
#include <array>
#include "Dispatcher.h"

class DispatcherImpl : public Dispatcher
{
public:
    using callback = std::function<void()>;

    template <typename F>
    void bind(MessageId messageId, F f)
    {
      subscribers[static_cast<size_t>(messageId)].emplace_back(std::forward<F>(f));
    }

    void dispatch(const Message& message)
    {
      const auto id = static_cast<size_t>(message.id);
      std::for_each(begin(subscribers[id]), end(subscribers[id]), std::mem_fn(&callback::operator()));
    }

private:
    using callbacks = std::vector<callback>;
    std::array<callbacks,static_cast<size_t>(MessageId::Count)> subscribers;
};

#endif // DISPATCHERIMPL_H
